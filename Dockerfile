FROM tomcat:8.5-alpine

ARG WD_VERSION
ENV JAVA_CONNECTOR_VERSION 2.3.0
RUN apk add curl unzip wget \
    && cd /usr/local/tomcat/webapps/ \
    && mkdir wombat \
    && cd wombat \
    && curl -vsL http://downloads.loway.ch/wd/WombatDialer-$WD_VERSION.tar.gz | tar --strip-components 1 -xz \
    && cd WEB-INF/lib/ \
    && wget https://downloads.mariadb.com/Connectors/java/connector-java-2.3.0/mariadb-java-client-${JAVA_CONNECTOR_VERSION}.jar
