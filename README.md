# Wombat Dialer for OMniLeads

This repository has the Dockerfile to build Wombat Dialer application

WombatDialer Version: 20.02.1-272
Base Image: tomcat:8.5-alpine

## Build

```
  docker build -t freetechsolutions/omldialer:$TAG .
```

Where $TAG is the docker tag you want for image.

## Run container

After build you can run your container:

```
  docker run -it freetechsolutions/omldialer:latest bash
```
